﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HiveConnect
{
    class Program
    {
        static void Main(string[] args)
        {
            // ================== Hive Parameters ===================
            
            string host = "10.210.99.162";
            int port = 10000;
            string username = "cloudera";
            string password = "cloudera";
                        
            var con = new Hive2.Connection(host, port, username, password);
            var cursor = con.GetCursor();

            // =========== Compare language references ==============
            
            cursor.Execute("use custom");
            cursor.Execute("SELECT * FROM stackover WHERE words IN ('python', 'java', 'c#', 'golang', 'scala')");
            
            var list = cursor.Fetch();
            
            List<string> keys = list.Columns[0].StringVal.Values;
            List<int> values = list.Columns[1].I32Val.Values;

            var dictionary = keys.ToDictionary(x => x, x => values[keys.IndexOf(x)]);

            foreach (var item in dictionary)
            {
                Console.WriteLine("{0} - {1}", item.Key, item.Value);
            }

            con.Close();
            
            // ========== What's more popural "a" or "the"? ============

            con = new Hive2.Connection(host, port, username, password);
            cursor = con.GetCursor();

            cursor.Execute("use custom");
            cursor.Execute("SELECT * FROM stackover WHERE words IN('a', 'the')");

            list = cursor.Fetch();

            keys = list.Columns[0].StringVal.Values;
            values = list.Columns[1].I32Val.Values;
            dictionary = keys.ToDictionary(x => x, x => values[keys.IndexOf(x)]);

            Console.WriteLine("\n==============");

            foreach (var item in dictionary)
            {
                Console.WriteLine("{0} - {1}", item.Key, item.Value);
            }

            con.Close();

            // =========== Words in database ============

            con = new Hive2.Connection(host, port, username, password);
            cursor = con.GetCursor();

            cursor.Execute("use custom");
            cursor.Execute("SELECT COUNT(*) FROM stackover");

            var result = cursor.Fetch().Columns[0].I64Val.Values[0];

            Console.WriteLine("\n==============");
            
            Console.WriteLine("Unique words at Stackoverflow forum - {0}", result);

            con.Close();

        }
    }
}
